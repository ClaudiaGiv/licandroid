# README #

The Android Application that has integrated the image processing component.

### What is this repository for? ###

This repository has been created in order to track my work on the thesis project and also as a backup storage. :D

### How do I get set up? ###

I order to set up this project you have to download and import it in Android studio. 
After that the project is ready to run on an emulator or directly on a smartphone connected to the computer.
In order for the app to work properly, that means that it has to have access to the database, so the backend app (Java app) should run on the server.

### Contribution guidelines ###

Write and document code.
Manual testing

### Who do I talk to? ###

Claudia Givan