#include <jni.h>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <ctime>
#include <iostream>
#include <queue>
#include <android/log.h>

using namespace cv;
using namespace std;

#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_SECURE_NO_WARNINGS

#define DEG2RAD  CV_PI/180 //0.017453293f
#define RAD2DEG 180/CV_PI //57.2957795f

int theta_max = 360;
int ro_max;
int tableRows, tableCols;
int len;
//int label;

typedef struct {
    int ro, t, val;
}Polar;

typedef struct {
    Point2f p1, p2;
}Line;

typedef struct {
    Mat img;
    int index;
    bool plain;
}Circle;


bool compare_by_intersections(Polar &a, Polar &b) {
    return a.val > b.val;
}

bool compare_by_x(Point2f &p1, Point2f &p2) {
    if (p1.x == p2.x || abs((p1.x - p2.x)) < 30) {
        return p1.y < p2.y;
    }
    return p1.x < p2.x;
}

bool compare_by_y(Point2f &p1, Point2f &p2) {

    if (p1.y == p2.y || abs((p1.y - p2.y)) < 30) {
        return p1.x < p2.x;
    }
    return p1.y < p2.y;
}

bool intersection(Line l1, Line l2, Point2f &r) {
    Point2f  x = l2.p1 - l1.p1;
    Point2f  d1 = l1.p2 - l1.p1;
    Point2f  d2 = l2.p2 - l2.p1;
    float cross = d1.x*d2.y - d1.y*d2.x;
    if (abs(cross) < 1e-8) {
        return false;
    }
    double t1 = (x.x*d2.y - x.y*d2.x) / cross;
    r = l1.p1 + d1*t1;
    return true;
}

double computeRad(int t) {
    return (double)t*DEG2RAD;
}

void accumulator(Mat src, Mat &Ho, Mat &Hv) {
    int height = src.rows;
    int width = src.cols;
    int i,j,val,t,ro;
    //the accumulator
    Ho = Mat::zeros(ro_max, theta_max, CV_16UC1);
    Hv = Mat::zeros(ro_max, theta_max, CV_16UC1);
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            val = src.at<uchar>(i, j);
            if (src.at<uchar>(i, j) == 255) {
                //compute all the lines that pass through the current point
                t=0,ro;
                for(t=0;t<=5;t++){
                    ro = j*cos(computeRad(t)) + i*sin(computeRad(t));
                    if (ro >= 0 && ro < ro_max) {
                        Hv.at<ushort>(ro, t)++;
                    }
                }
                for(t=85;t<=95;t++){
                    ro = j*cos(computeRad(t)) + i*sin(computeRad(t));
                    if (ro >= 0 && ro < ro_max) {
                        Ho.at<ushort>(ro, t)++;
                    }
                }
                for(t=175;t<=185;t++){
                    ro = j*cos(computeRad(t)) + i*sin(computeRad(t));
                    if (ro >= 0 && ro < ro_max) {
                        Hv.at<ushort>(ro, t)++;
                    }
                }
                for(t=265;t<=275;t++){
                    ro = j*cos(computeRad(t)) + i*sin(computeRad(t));
                    if (ro >= 0 && ro < ro_max) {
                        Ho.at<ushort>(ro, t)++;
                    }
                }
                for(t=355;t<theta_max;t++){
                    ro = j*cos(computeRad(t)) + i*sin(computeRad(t));
                    if (ro >= 0 && ro < ro_max) {
                        Hv.at<ushort>(ro, t)++;
                    }
                }
            }
        }
    }
}

Point findMax(Mat H)
{
    int i, j, height = H.rows, width = H.cols;
    ushort max = H.at<ushort>(0,0);
    Point pMax;
    pMax.x = 0;
    pMax.y = 0;
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            if (H.at<ushort>(i, j) > max) {
                max = H.at<ushort>(i, j);
                pMax.x =i;
                pMax.y =j;
            }
        }
    }
    return pMax;
}

int computeY(int x, int r, int t) {
    vector<Line> lines2;
    double tR = computeRad(t);
    int y = (r - x*cos(tR)) / sin(tR);
    return y;
}

int computeX(int y, int r, int t) {
    vector<Line> lines2;
    double tR = computeRad(t);
    int x = (r - y*sin(tR)) / cos(tR);
    return x;
}

vector<Polar> findVLines(Mat src, Mat &Hv, int vThreshold, int nrV) {
    vector<Polar> lines;
    Point vMax;
    int width = src.cols;
    int height = src.rows;
    int t, ro;
    bool stop = false;
    while (nrV > 0 && !stop) {
        vMax = findMax(Hv);
        if (Hv.at<ushort>(vMax.x, vMax.y) > vThreshold)
        {
            Polar p;
            p.ro = vMax.x;
            p.t = vMax.y;
            p.val = Hv.at<ushort>(vMax.x, vMax.y);
            lines.push_back(p);
            for (int y = 0; y < height; y++) {
                int x1 = computeX(y, p.ro, p.t);
                int xi = x1 - 10, xf = x1 + 10;
                xi < 0 ? xi = 0 : xi = xi;
                xf > width ? xf = width - 1 : xf = xf;
                for (int x = xi; x <= xf; x++) {
                    for (t = 0; t <= 5; t++) {
                        ro = x*cos(computeRad(t)) + y*sin(computeRad(t));
                        if (ro >= 0 && ro < ro_max && Hv.at<ushort>(ro, t) > 0) {
                            Hv.at<ushort>(ro, t)--;
                        }
                    }
                    for (t = 175; t <= 185; t++) {
                        ro = x*cos(computeRad(t)) + y*sin(computeRad(t));
                        if (ro >= 0 && ro < ro_max && Hv.at<ushort>(ro, t) > 0) {
                            Hv.at<ushort>(ro, t)--;
                        }
                    }
                    for (t = 355; t<theta_max; t++) {
                        ro = x*cos(computeRad(t)) + y*sin(computeRad(t));
                        if (ro >= 0 && ro < ro_max && Hv.at<ushort>(ro, t) > 0) {
                            Hv.at<ushort>(ro, t)--;
                        }
                    }
                }
            }
            nrV--;
        }
        else {
            stop = true;
        }
    }
    return lines;
}

vector<Polar> findOLines(Mat src, Mat &Ho, int oThreshold, int nrO) {
    vector<Polar> lines;
    Point oMax;
    int width = src.cols;
    int height = src.rows;
    int t, ro;
    bool stop = false;
    while (nrO > 0 && !stop) {
        oMax = findMax(Ho);
        if (Ho.at<ushort>(oMax.x,oMax.y) > oThreshold)
        {
            Polar p;
            p.ro = oMax.x;
            p.t = oMax.y;
            p.val = Ho.at<ushort>(oMax.x, oMax.y);
            lines.push_back(p);
            for (int x = 0; x < width; x++) {
                int y1 = computeY(x, p.ro, p.t);
                int yi = y1 - 10, yf = y1 + 10;
                yi < 0 ? yi = 0 : yi = yi;
                yf > height ? yf = height-1 : yf = yf;
                for (int y = yi; y <= yf; y++) {
                    for (t = 85; t <= 95; t++) {
                        ro = x*cos(computeRad(t)) + y*sin(computeRad(t));
                        if (ro >= 0 && ro < ro_max && Ho.at<ushort>(ro, t) > 0) {
                            Ho.at<ushort>(ro, t)--;
                        }
                    }
                    for (t = 265; t <= 275; t++) {
                        ro = x*cos(computeRad(t)) + y*sin(computeRad(t));
                        if (ro >= 0 && ro < ro_max && Ho.at<ushort>(ro, t) > 0) {
                            Ho.at<ushort>(ro, t)--;
                        }
                    }
                }
            }
            nrO--;
        }
        else {
            stop = true;
        }
    }
    return lines;
}

int findThreshold(Mat img) {
    //int* hist = calcHist(img);

#pragma region OpenCvHistogram
    int channels[] = { 0 };
    int histSize[] = { 256 };
    float range[] = { 0, 256 };
    const float* ranges[] = { range };
    Mat hist;
    calcHist(&img, 1, channels, Mat(), // do not use mask
             hist, 1, histSize, ranges,
             true, // the histogram is uniform
             false);
#pragma endregion

    const int L = 256; //max level of intesities
    /*for (int i = 0; i < hist.rows; i++) {
    for (int j = 0; j < hist.cols; j++) {
    cout << i << " : " << hist.at<float>(i, j) << "   ";
    }
    cout << endl;
    }*/

    int height = img.rows;
    int width = img.cols;
    double M = height*width; //no. of pixels
    int A[L] = { 0 };
    A[0] = M;
    int max = -1, T = 0;;
    for (int i = 0; i < L - 1; i++) {
        A[i + 1] = A[i] - hist.at<float>(i, 0);
        if (A[i] * i > max) {
            max = A[i] * i;
            T = i;
        }
    }
    return T;

}

vector<Line> drawLines(vector<Polar> lines, Mat dst_color) {
    vector<Line> lines2;
    int w = dst_color.step[0];
    int lx = w / 2 + 10;
    //float x11 = 0, y11, x12 = src.step, y12;
    for (int i = 0; i < lines.size(); i++) {
        Polar line1 = lines.at(lines.size() - i - 1);
        Point pt1, pt2;
        //daca pun si conditia || (line1.t >=260 && line1.t <= 280) atunci o sa se suprapuna unele lini cu aproape exact aceleasi coordonate
        //if ((line1.t >= 0 && line1.t < 10) || (line1.t > 80 && line1.t < 100) || (line1.t > 350 && line1.t < 360)) {
        //cout << line1.t * DEG2RAD << "===" << line1.t << endl;
        double a = cos((double)line1.t * DEG2RAD), b = sin((double)line1.t * DEG2RAD);
        double x0 = a*line1.ro, y0 = b*line1.ro;
        pt1.x = cvRound(x0 + lx * (-b));
        pt1.y = cvRound(y0 + lx * (a));
        pt2.x = cvRound(x0 - lx * (-b));
        pt2.y = cvRound(y0 - lx * (a));
        /*pt1.x = x11;
        pt1.y = (line1.ro - x11*a)/b;
        pt2.x = x12;
        pt2.y = (line1.ro - x12*a) / b;*/
        Line l;
        l.p1 = pt1;
        l.p2 = pt2;
        lines2.push_back(l);
        //cout << pt1 << "--" << pt2 << endl;
        line(dst_color, pt1, pt2, Scalar(0, 0, 255), 3, CV_AA);
        //}
    }
    return lines2;
}

vector<Point2f> drawCircles(vector<Line> oLines, vector<Line> vLines, Mat dst_color) {
    vector<Point2f> intersections;
    int height = dst_color.rows, i;
    int width = dst_color.cols, j;
    Point2f r;
    for (i = 0; i < oLines.size(); i++) {
        for (j = 0; j < vLines.size(); j++) {
            Line l1 = oLines[i];
            Line l2 = vLines[j];
            if (intersection(l1, l2, r)) {
                if (r.x >= 10 && r.x < width-10 && r.y >= 10 && r.y < height-10) {
                    circle(dst_color, r, 3, Scalar(0, 255, 0), 3, CV_AA);
                    intersections.push_back(r);
                }
            }
        }
    }
    return intersections;
}

void tableDetails(vector<Point2f> intersections) {
    int rows = 0, cols = 0;
    Point2f current = intersections.at(0);
    for (int j = 1; j < intersections.size(); j++) {
        Point2f point = intersections.at(intersections.size() - j - 1);
        if (point.x == current.x || abs(point.x - current.x) <= 30) {
            rows++;
        }
        if (point.y == current.y || abs(point.y - current.y) <= 30) {
            cols++;
        }
    }
    tableRows = rows;
    tableCols = cols;

}

//Labeling algorithm from PI labs
int labeling(Mat src, Mat &labels) {
    int di[8] = {-1, -1, -1, 0, 1, 1, 1, 0};
    int dj[8] = {1, 0, -1, -1, -1, 0, 1, 1};
    queue<Point2i> q;
    int height = src.rows, i, i1;
    int width = src.cols, j, j1;
    labels = Mat::zeros(height, width, CV_8UC1);
    int label = 0, k, maxLabel = 0, contor = 0, maxContor=0;
    for (i = 10; i < height - 8; i++)
    {
        for (j = 10; j < width - 8; j++)
        {
            if (src.at<uchar>(i, j) == 0 && labels.at<uchar>(i, j) == 0)
            {
                label++;
                contor = 0;
                Point pt;
                pt.x=j, pt.y=i;
                q.push(pt);
                labels.at<uchar>(i, j) = label;
                contor++;
                //cout << i << " xxx " << j << "  " << label << endl;
                while (!q.empty())
                {
                    Point2i p = q.front();
                    q.pop();
                    i1 = p.y;
                    j1 = p.x;
                    for (k = 0; k < 8; k++)
                    {
                        Point2i p1;
                        p1.x = j1 + dj[k];
                        p1.y = i1 + di[k];
                        if (src.at<uchar>(i1 + di[k], j1 + dj[k]) == 0 && labels.at<uchar>(i1 + di[k], j1 + dj[k]) == 0)
                        {
                            labels.at<uchar>(i1 + di[k], j1 + dj[k]) = label;
                            contor++;
                            q.push(p1);
                        }
                    }
                }
                if(contor > maxContor)
                {
                    maxContor = contor;
                    maxLabel = label;
                }
            }
        }
    }
    return maxLabel;
}

int computeArea(Mat labels, int label) {
    int height = labels.rows;
    int width = labels.cols;
    int area = 0, i, j;
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            if (labels.at<uchar>(i, j) == label) {
                area++;
            }
        }

    }
    return area;
}

Point computeCenter(Mat labels, int label, int area) {
    int height = labels.rows;
    int width = labels.cols;
    int sx = 0, sy=0, i, j;
    Point center;
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            if (labels.at<uchar>(i, j) == label) {
                sx += j;
                sy += i;
            }
        }

    }
    center.x = (1.0 / area) * sx;
    center.y = (1.0 / area) * sy;
    return center;
}

vector<Circle> roi(vector<Point2f> intersections, Mat img, Mat colorSrc) {

    vector<Circle> plainCircles;
    tableDetails(intersections);
    vector<Point2f> intersections_x = intersections;
    sort(intersections_x.begin(), intersections_x.end(), compare_by_x);
    vector<Point2f> intersections_y = intersections;
    sort(intersections_y.begin(), intersections_y.end(), compare_by_y);
    String s;
    Point2f p1y = intersections_y[0];
    Point2f p1x = intersections_x[0];
    Point2f p2x = intersections_x[1];
    Point2f p2y = intersections_y[1];
    int difx = abs(p1y.x - p2y.x)+3;
    int dify = abs(p1x.y - p2x.y) +3;
    Mat rect;
    vector<Mat> rois;
    int k = tableCols, k1=0;
    for (int i = 0; i < tableRows - 2; i++) {
        for (int j = 1; j < tableCols - 1; j++) {
            Point2f point = intersections_y.at(k + j);
            rect = img(Rect((int)point.x, (int)point.y, difx -1, dify - 1));
            Mat rectColor;
            cvtColor(rect, rectColor, COLOR_GRAY2BGR);
            Mat labels;
            int label = labeling(rect, labels);
            int area = computeArea(labels, label);
            Point center = computeCenter(labels, label, area);
            int i1 = center.y, j1 = center.x;
            /*if (i == 0 || j == 0) {
                continue;
            }*/
            int s = 0;
            for (int t = -3; t <= 3; t++) {
                for (int u = -3; u <= 3; u++) {
                    if (labels.at<uchar>(i1 + t, j1 + u) == label) {
                        s++;
                    }
                }
            }
            Circle c;
            c.img = img;
            c.index = k1;
            if (s != 0) {
                Point np;
                np.x= center.x + point.x;
                np.y = center.y + point.y;
                circle(colorSrc, np, 18, Scalar(255, 0, 0), 2, CV_AA);
                //rois.at(k) = img;
                c.plain = true;
                plainCircles.push_back(c);
            }
            rois.push_back(rect);
            k1++;
        }

        k += tableCols;
    }
    return plainCircles;
}

vector<Circle> findPlainCircles(vector<Mat> rois, Mat colorSrc) {
    vector<Circle> plainCircles;
    int k, height, width, i, j, area = 0, s=0;
    int di[8] = { -1, 0, 1, 0};
    int dj[8] = { 0, 1, 0, -1};
    bool stop = false;
    for (k = 0; k < rois.size(); k++) {
        Mat img = rois.at(k);
        Mat labels;
        int label = labeling(img, labels);
        area = computeArea(labels, label);
        Point center = computeCenter(labels, label, area);
        i = center.y, j = center.x;
        if (i == 0 || j == 0) {
            continue;
        }
        s = 0;
        for (int t = -3; t <= 3; t++) {
            for (int u = -3; u <= 3; u++) {
                if (labels.at<uchar>(i + t, j + u) == label) {
                    s++;
                }
            }
        }
        Circle circle;
        circle.img = img;
        circle.index = k;
        if (s!=0) {
            circle.plain = true;
            plainCircles.push_back(circle);
        }
    }
    return plainCircles;
}


jint * hough(Mat src1, Mat &color) {

    clock_t begin_time = clock();

    __android_log_print(ANDROID_LOG_INFO, "LOG_TAG", "\n SIZEX: %d SIZEX: %d \n", src1.cols,src1.rows);
    //int x = src1.cols / 4;
    //int y = src1.rows / 4;
    Mat resizedSrc = src1.clone();
    //resize(src1, resizedSrc, Size(x, y));
    int w = resizedSrc.step;

    Mat dst = resizedSrc.clone();
    Mat edgeSrc, binarizedImg;
    Mat colorSrc = Mat::zeros(dst.rows, dst.cols, CV_8UC3);

    //convert to colored image ( in order to draw colored lines and points)
    cvtColor(dst, colorSrc, COLOR_GRAY2BGR);

    __android_log_print(ANDROID_LOG_INFO, "LOG_TAG", "\n CVTCOLOR: %f \n", float( clock () - begin_time ) /  CLOCKS_PER_SEC);

    begin_time = clock();
    int threashold = findThreshold(resizedSrc);
    double percentT = 80.0/100;

    threashold = percentT * threashold;
    threshold(resizedSrc, binarizedImg, threashold, 255, THRESH_BINARY);
    Canny(binarizedImg, edgeSrc, 50, 200, 3);

    int height = resizedSrc.rows;
    int width = resizedSrc.cols;

    //se the ro_max global variable
    ro_max = sqrt(height*height + width*width) + 1;

    __android_log_print(ANDROID_LOG_INFO, "LOG_TAG", "\n CANNY: %f \n", float( clock () - begin_time ) /  CLOCKS_PER_SEC);
    //compute the accumulators
    Mat Ho, Hv;
    begin_time = clock();
    accumulator(edgeSrc, Ho, Hv);

    __android_log_print(ANDROID_LOG_INFO, "LOG_TAG", "\n ACCUMULATOR: %f \n", float( clock () - begin_time ) /  CLOCKS_PER_SEC);
    //find the correct lines
    double percentVLines = 15.0/ 100;
    double percentOLines = 20.0 / 100;
    int nrV = 40, nrO=10, oT= percentOLines * width, vT= percentVLines * height;
    begin_time = clock();
    vector<Polar> polarOLines = findOLines(edgeSrc, Ho, oT, nrO);
    __android_log_print(ANDROID_LOG_INFO, "LOG_TAG", "\n FINDOLINES: %f \n", float( clock () - begin_time ) /  CLOCKS_PER_SEC);
    begin_time = clock();
    vector<Polar> polarVLines = findVLines(edgeSrc, Hv, vT, nrV);

    __android_log_print(ANDROID_LOG_INFO, "LOG_TAG", "\n FINDVLINES: %f \n", float( clock () - begin_time ) /  CLOCKS_PER_SEC);
    begin_time = clock();
    //draw lines and retrieve the cartezian defined lines
    vector<Line> oLines = drawLines(polarOLines, colorSrc);
    vector<Line> vLines = drawLines(polarVLines, colorSrc);
    __android_log_print(ANDROID_LOG_INFO, "LOG_TAG", "\n DRAWLINES: %f \n", float( clock () - begin_time ) /  CLOCKS_PER_SEC);
    __android_log_print(ANDROID_LOG_INFO, "LOG_TAG", "\n OLines: %d \n", oLines.size() );
    __android_log_print(ANDROID_LOG_INFO, "LOG_TAG", "\n VLines: %d \n", vLines.size() );

    //draw circles and retrieve intersection points
    vector<Point2f> intersections = drawCircles(oLines, vLines, colorSrc);

    //retrieve the regions of interest = rectangular regions from table defined by the intersection points
    vector<Circle> circles = roi(intersections, binarizedImg,colorSrc);
    color = colorSrc;
    //vector<Circle> circles = findPlainCircles(rois);
    __android_log_print(ANDROID_LOG_INFO, "LOG_TAG", "TOOOMUCH ");
    //return colorSrc;
    jint* plainCircles = new jint[50];
    int i;
    for(i=0;i< circles.size();i++){
        plainCircles[i] = circles.at(i).index;
    }
    len=i;
    __android_log_print(ANDROID_LOG_INFO, "LOG_TAG", "TABLEXXX %d",plainCircles[2]);
    return plainCircles;
}

extern "C"
{

    char* type2str(const Mat &mat) {
        int type = mat.type();

        switch(type){
            case CV_8UC1:
                return "CV_8UC1";
            case CV_8UC2:
                return "CV_8UC2";
            case CV_8UC3:
                return "CV_8UC3";
            case CV_8UC4:
                return "CV_8UC4";
            case CV_8SC1:
                return "CV_8SC1";
            case CV_8SC2:
                return "CV_8SC2";
            case CV_8SC3:
                return "CV_8SC3";
            case CV_8SC4:
                return "CV_8SC4";
            case CV_32FC1:
                return "CV_32FC1";
            case CV_32FC2:
                return "CV_32FC2";
            case CV_32FC3:
                return "CV_32FC3";
            case CV_32FC4:
                return "CV_32FC4";
            case CV_64FC1:
                return "CV_32FC1";
            case CV_64FC2:
                return "CV_32FC2";
            case CV_64FC3:
                return "CV_32FC3";
            case CV_64FC4:
                return "CV_32FC4";
            default:
                return "Unknown";
        }
    }

    void JNICALL Java_com_example_clau_myapplication_activity_CameraActivity_salt(JNIEnv *env, jobject instance,
                                                                       jlong matAddrGray,
                                                                       jint nbrElem) {
        Mat &mGr = *(Mat *) matAddrGray;
        for (int k = 0; k < nbrElem; k++) {
            int i = rand() % mGr.cols;
            int j = rand() % mGr.rows;
            mGr.at<uchar>(j, i) = 255;
        }
    }

    JNIEXPORT void JNICALL Java_com_example_clau_myapplication_activity_CameraActivity_ProcessTest(JNIEnv *env, jobject instance,
                                                                  jlong matAddrGr, jlong matAddrRgba,jint x, jint y, jint width, jint height){

        Mat &mGr = *(Mat *) matAddrGr;
        Mat &mRgb = *(Mat *) matAddrRgba;
        __android_log_print(ANDROID_LOG_INFO, "LOG_TAG", "\n MRGBA : %s \n",type2str(mRgb));
       // mRgb=mGr.clone();
        //int x=mGr.cols;
        //int y=mGr.rows;
        Mat r = mRgb(Rect(x,y,width,height));
        Mat a =r.clone();
        Mat g = mGr(Rect(x,y,width,height));
        clock_t begin_time = clock();
        //resize(mGr,mGr,Size(x/2,y/2));
        //resize(mRgb,mRgb,Size(x/2,y/2));
        __android_log_print(ANDROID_LOG_INFO, "LOG_TAG", "\n Resize11111: %f \n", float( clock () - begin_time ) /  CLOCKS_PER_SEC);
        //cvtColor(mRgb,mRgb,CV_BGRA2BGR);
        //a = hough(g);
        cvtColor(a,a,CV_BGR2BGRA);
        __android_log_print(ANDROID_LOG_INFO, "LOG_TAG", "\n AAAAAAAAAAA %s \n", type2str(a));
        a.copyTo(mRgb(Rect(x,y,width,height)));
        begin_time = clock();
        //resize(mGr,mGr,Size(x,y));
        //resize(mRgb,mRgb,Size(x,y));
        __android_log_print(ANDROID_LOG_INFO, "LOG_TAG", "\n Resize222: %f \n", float( clock () - begin_time ) /  CLOCKS_PER_SEC);

    }

    JNIEXPORT jintArray JNICALL
    Java_com_example_clau_myapplication_activity_CameraActivity_TableLines(JNIEnv *env, jobject instance,
                                                                jlong matAddrGr, jlong matAddrRgba,jint x, jint y, jint width, jint height) {
        Mat &mGr = *(Mat *) matAddrGr;
        Mat &mRgb = *(Mat *) matAddrRgba;
       // Mat r = mRgb(Rect(x,y,width,height));
       // Mat g = mGr(Rect(x,y,width,height));
        clock_t begin_time = clock();
      //  cvtColor(r,r,CV_BGR2BGRA);
        jintArray  outJNIArray;
        jint * circles = hough(mGr, mRgb);
        jintArray circles1 = env->NewIntArray(len);
        env->SetIntArrayRegion(circles1, (jsize)0, (jsize)len, circles);
        __android_log_print(ANDROID_LOG_INFO, "LOG_TAG", "XXXEE %d",circles[1]);
        return circles1;
    }
}